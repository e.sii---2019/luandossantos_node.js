var mongoose = require('mongoose');
var db;

//criado comunicação com o banco de dados
module.exports = function(){
    if (!db) {
        db = mongoose.createConnection('mongodb://localhost/loja');
    }
    return db;
}