module.exports = function(){
    var db = require('./../libs/connect_db')();
    var Schema = require('mongoose').Schema;

    var task = Schema({
        produto: String,
        marca: String,
        preco: Number
    });

    return db.model('tasks', task);
}