# Trabalho de Engenharia de Software 
Tecnologias utilizados no projeto:
- MongoDB
- Node.js
- CSS
- JavaScript
- HTML

Link do vídeo onde eu falo um pouco sobre o Node.js e explico o Crud: https://youtu.be/74rVciLI1Qw

Como executar a aplicação em sua máquina:

1. Fazer o download do Node.js pelo site (https://nodejs.org/en/)
2. Fazer o download do MogoDB e fazer a configuração do mesmo atravez desse link (https://www.youtube.com/watch?v=skK5xj-CK-Q).
3. Clonar o projeto para a sua máquina e logo após abrir ele em uma ferramenta de edição (Visual Studio Code).
4. Abra o cmd e utilize o comando "mongod" dentro da pasta raiz do projeto para iniciar o servidor do mongo.
5. Logo após abra outro cmd e vá até a pasta raiz do projeto novamente e execute o comando npm start para rodar o projeto (com isso quando você entrar no browse irá criar o banco de dados automaticamente).
6. Para rodar abra no navegador de sua escolha e na barra de pesquisa utilize "http://localhost:3000/"
7. Com isso abrirá o projeto e estará pronto pata ser utilizado